import React from 'react'

//Classe para exportar mais de uma função
export const BoaTarde = props => <h1>Boa Tarde {props.nome}!</h1>

export const BoaNoite = props => <h1>Boa Noite {props.nome}!</h1>

//Pode ser feito assim também mas na hora de importar tem que tirar das {}
//export default BoaTarde

//pode ser exportado assim também assim tem que acessar com o '.' ou usar {}
//export default {BoaTarde, BoaNoite}