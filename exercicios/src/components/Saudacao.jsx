import React, {Component} from 'react'

export default class Saudacao extends Component {

    //Para implementar essa função render se espera receber 2 parametros 1 é o tipo de saudação 
    //e o outro é o nome da pessoa referenciada pela saudação e os parametros ficam dentro de props.
    
    //state pode ser alterado props não
    state = {
        tipo: this.props.tipo,
        nome: ""
    }

    //
    setTipo(e){
       // sempre que quiser alterar o estado tem que usar essa função setState
       this.setState({tipo:  e.target.value} 
        /* aqui passa objeto com um ou mais atributos que pode alterar o estado do objeto, esse estado pertence ao 
        objeto que se está trabalhando, se colocar mais de um objeto saudação, cada objeto tem que ter seu estado */)
    }

    setNome(e){
        this.setState({nome: e.target.value})
    }

    //Função responsavel por renderizar o componente
    render(){
        //No componente funcional funcional recebe props diretamente como aqui é um componente de classe
        //se usa this.props.
        
        //aqui está sendo usado o operador destructor
        const {tipo, nome } = this.state
        return(
            <div>
                <h1>{tipo} {nome}!</h1>
                <hr />
                <input type="text" placeholder="Tipo..." value={tipo} onChange={e => this.setTipo(e)}/>
                
                <input type="text" placeholder="Nome..." value={nome} onChange={e => this.setNome(e)} />
            </div>
        )
    }
}