import React, {Fragment}from 'react'

//esse props serve para pegar a propriedade passada
export default props =>
    //Fragment serve para colocar as tags dentro mas ao contrario de 
    //div não é renderizado junto    
    <Fragment>
        <h1>Bom dia {props.nome}</h1>
        <h2>até breve</h2>
    </Fragment>

// pode ser usado array também, só tem que ter chave 
//export default props => [   
//    <h1 key='k1'>Bom dia {props.nome}</h1>,
//    <h2 key='k2'>até breve</h2> ]