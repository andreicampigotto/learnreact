import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

//{} é operador de desestruturação
import {BoaTarde, BoaNoite} from './components/Multiplos'

//import para o componente saudação, pode  se colocar tipo e nome
import Saudacao from './components/Saudacao'

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
