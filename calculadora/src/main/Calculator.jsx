import React, { Component } from 'react'
import './Calculator.css'

import Button from '../components/Button'
import Display from '../components/Display'

/*essa constante representa o estado inicial*/
const initialState = {
    displayValue: '0',
    clearDisplay: false,
    /*varivel que armazena operação*/
    operation: null,
    /*arrey que guarda o valor para poder a operação */
    values: [0, 0],
    /*esse parametro diz qual o valor do arrey está sendo manipulado*/
    current: 0
}

export default class Calculator extends Component {

    state = { ...initialState }

    constructor(props) {
        super(props)
        /*essas funções são construtores para o render*/ 
        this.clearMemory = this.clearMemory.bind(this)
        this.setOperation = this.setOperation.bind(this)
        this.addDigit = this.addDigit.bind(this)
    }

    clearMemory() {
        this.setState({ ...initialState })
    }

    setOperation(operation) {
        /*Essa condição verifica o número para a operação esta na posição 0 do arrey */
        if (this.state.current === 0) {
            this.setState({ operation, current: 1, clearDisplay: true })
        } 
        /*Essa condição verifica se foi clicado no '=', currentOperation verifica se foi clicado em outra operação */
        else {
            const equals = operation === '='
            const currentOperation = this.state.operation

            //Esse values é um clone de ...this.state.values
            const values = [...this.state.values]
            try {
                //Essa condição pega o valor da posição 0 do array e 1 e fazer a operação com o valor que 
                //tiver em currentOperation e vai armazenar o resultado no indice 0
                values[0] = eval(`${values[0]} ${currentOperation} ${values[1]}`)
            } catch(e) {
                values[0] = this.state.values[0]
            }

            //aqui zera o indice 1 do array
            values[1] = 0

            //
            this.setState({
                //aqui armazena o resultado da operação no display
                displayValue: values[0],
                //aqui verifica a operação se for equals seta como nula 
                operation: equals ? null : operation,
                //aqui verifica se o usuario clocou em equals continua mexendo no 0 se não ele mexe no 1 valor
                current: equals ? 0 : 1,
                //aqui não deixa limpar o display se foi clicado em equals
                clearDisplay: !equals,
                //aqui mostra os valores para serem substituidos no estadogit 
                values
            })
        }
    }

    addDigit(n) {
        /*Essa condição impede que tenha mais de 1 ponto na calculadora*/ 
        if (n === '.' && this.state.displayValue.includes('.')) {
            return
        }

        /*Essa condição tira o 0 e coloca outro valor quando se digita outro valor,
        ou quando a clearDisplay estiver true*/
        const clearDisplay = this.state.displayValue === '0'
            || this.state.clearDisplay
        /*Essa variavel verifica se o display vai ser limpo*/
        const currentValue = clearDisplay ? '' : this.state.displayValue
        /*Essa constante contem o novo valor para o display e já passou pelas contantes de cima, 
        ela vai pegar o currentValue + o numero(n) e muda o estado setState({ displayValue, clearDisplay: false })*/
        const displayValue = currentValue + n
        this.setState({ displayValue, clearDisplay: false })

        /**/
        if (n !== '.') {
            /*Esse I armazena o indice do array que esta sendo mexido*/
            const i = this.state.current
            /*Essa constante converte a string para float*/
            const newValue = parseFloat(displayValue)
            /*Essa constante clona o valor e coloca o valor no indice que está sendo mexido*/
            const values = [...this.state.values]
            values[i] = newValue
            /*adiciona o novo valor dentro do stateg*/
            this.setState({ values })
        }
    }

    render() {
        return (
            <div className="calculator">
                {/*o display agora aponta para o estado não mais para valor fixo*/}
                <Display value={this.state.displayValue} />
                <Button label="AC" click={this.clearMemory} triple />
                <Button label="/" click={this.setOperation} operation />
                <Button label="7" click={this.addDigit} />
                <Button label="8" click={this.addDigit} />
                <Button label="9" click={this.addDigit} />
                <Button label="*" click={this.setOperation} operation />
                <Button label="4" click={this.addDigit} />
                <Button label="5" click={this.addDigit} />
                <Button label="6" click={this.addDigit} />
                <Button label="-" click={this.setOperation} operation />
                <Button label="1" click={this.addDigit} />
                <Button label="2" click={this.addDigit} />
                <Button label="3" click={this.addDigit} />
                <Button label="+" click={this.setOperation} operation />
                <Button label="0" click={this.addDigit} double />
                <Button label="." click={this.addDigit} />
                <Button label="=" click={this.setOperation} operation />
                
            </div>
        )
    }
}