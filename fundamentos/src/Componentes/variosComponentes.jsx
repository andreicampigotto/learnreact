import React from 'react'

const CompA = props => 
    <h1>Primeiro dix: {props.valor}</h1>

const CompB = props =>
    <h1>Segundo dix: {props.valor}</h1>

export {CompA, CompB}
export default CompA
