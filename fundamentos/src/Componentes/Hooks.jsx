import React, {useState, useEffect} from 'react'

export default props => {
    //const [contador, setContador] = useState(100)
    //nome do primeiro elemento armazena o valor, e o segundo exatamente o nome da função que irá alterar o valor do primeiro prime 
    
    //[contador, setContador] é um operador de desestruturação, 
    //useState é um hook de controle de estado do componente
    //se usa passando um valor inicial e se recebe 2 coisas o 
    //primeiro elemento é exatamente o valor e o segundo é o valor para alterar o estado 
    //o segundo elemento altera o estado definido 
    const [contador, setContador] = useState(100)
    //ele possibilita pegar os elementos a partir do indice ou como lista;


    //Metodo de ciclo de vida
    //Com useEffect chama a função sempre que o componente é montado e atualizado
    //stat sempre será indice 0 é o valor de dentro do componente para ler o estado
    //setParouImpar sempre será indice 1 sempre é a função que altera o estado
    const [status, setParouImpar]=useState('Par')
    
    //Sempre que for atualizado vai executar o useEffect
    useEffect (() => {
        contador % 2 === 0 ? 
            setParouImpar('Par') :
            setParouImpar('Impar')
    }) 

    return ( 
        <div>
            <h1>{contador}</h1>
            <h3>{status}</h3>
            <button 
            onClick={() => setContador(contador +1)}>
                Inc
            </button>
        </div>
    )
}