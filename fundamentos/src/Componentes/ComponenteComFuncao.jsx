import React from 'react'

export default props => {
    const aprovados = ['Josefina', 'analfa', 'pietra', 'Ariel']
    let num = Math.random()

    //isso é uma função
    const gerarItens = itens => {
        return itens.map(item => <li>{item}</li>)
    }

    return(
        <ul>
            {num}
            {gerarItens(aprovados)}
        </ul>
    )
}