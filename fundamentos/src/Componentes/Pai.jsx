import React from 'react'
import Filho from './Filho'

export default props => {
    const notificarSaidadoFilho = 
        lugar => {alert(`liberado para ${lugar}`)
        }
    return(
        <div>
            <Filho notificarSaida={notificarSaidadoFilho} />
        </div>
    )
}