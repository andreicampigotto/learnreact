import React, {Component} from 'react'

export default class Contador extends Component{
    /* foi mudado de props para state*/ 
    state ={
        numero: 0
    }

    maisUm = () =>{
        //passando apenas os atributos que quero modificar
        // para dentro do setState 
        this.setState({numero: this.state.numero + 1})
        //this.state.numero++
    }

    menosUm = () => {
        this.alterarNumero(-1)
    }

    alterarNumero = (diferenca) => {
        this.setState({
            numero: this.state.numero + diferenca})
    }
    
    render(){
        return(
            <div>
                <div>Número: {this.state.numero}</div>
                <button onClick={this.maisUm}>Incrementar</button> {/*se colocar this.maisUm() chama o retorno da função não a função*/}
                <button onClick={this.menosUm}>Decrementar</button>
                <button onClick={() => this.alterarNumero(10)}>Mais 10</button>
            </div>
        )
    }

    /* Solução 01
    constructor (props){
        {/*contrutor da função, chama o super para passar as 
        propriedades para a classe pai (component).
        bind garante que o contrutor aponta para o contador
        usando construtor e bind para garantir que em qualquer
        contexto o this aponta para o objeto atual}
        super(props)
        this.maisUm = this.maisUm.bind(this)
    }*/

    /* Solução 02
        função arrow no on Click(()=> garante que o .this será 
        associado ao local que a função foi escrita)  
        <button onClick={() => this.maisUm()}>Incrementar</button>
    */ 

    /*Solução 03
    Transformar a função em uma função Arrow (=>) ela trata o 
    this no (contexto lexico) ela associal ao local que a função 
    foi escrita dentro do codico, como foi escrita dentro da classe 
    contador o this aponta pra contador porque a função está dentro 
    da classe não importando quem chamou a função sempre será resolvido
    para a instancia da classe contador. 
    maisUm = () =>{
        this.props.numero++
    }*/ 
}