import React from 'react'
import ReactDOM from "react-dom"
//import FirstComponent from './Componentes/FirstComponents'
//import {CompA, CompB as B} from './Componentes/variosComponentes'
//import M from './Componentes/multiElementos'
//import FamiliaSilva from './Componentes/FamiliaSilva'
//import Famila from './Componentes/Familia'
//import Membro from './Componentes/Membro'
//import ComponenteComFuncao from './Componentes/ComponenteComFuncao'
//import Pai from './Componentes/Pai'
//import ComponenteClasse from './Componentes/ComponenteClasse'
//import Contador from './Componentes/Contador'
import Hook from './Componentes/Hooks'

const elemento = document.getElementById('root')
ReactDOM.render(
    <div>
        <Hook/>
        {/*<Contador numero={0}/>*/}
        {/*<ComponenteClasse valor="Esse é um componente Classe"/>*/}
        {/*<Pai />*/}
        {/*<ComponenteComFuncao/>*/}
        {/*<Famila sobrenome="Jensen">
            <Membro name="Ariel"  />
            {<Membro name="Josefina" />}
        </Famila>*/}

        {/*<Famila>
            <Membro name="Ariana" sobrenome="Jensen" />
            <Membro name="Zé" sobrenome="Silva"/>
        </Famila>    */}

        {/*<FamiliaSilva/>*/}
        {/*<M/>*/}
        {/*<FirstComponent valor="Boa tarde"/>*/}
        {/*<CompA valor= "Esse é o comp A"/>
        <B valor= "Esse é o comp B"/>*/}

    </div>
, elemento)
