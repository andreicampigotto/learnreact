import React, {Component} from 'react'
import axios from 'axios';

import PageHeader from '../template/pageHeader'
import TodoForm from './todoForm'
import TodoList from './todoList'

const URLO = 'http://localhost:3003/api/todos'

//Essa Classe todo fica concentrada toda a logica de eventos e estado
export default class Todo extends Component{
    constructor(props){
        super(props) //props somente leitura
        this.state = { description: '', list: []} //estado inicial do objeto

        this.handleChange = this.handleChange.bind(this) //amarrando que o bind sempre será o atual
        this.handleAdd = this.handleAdd.bind(this) //bind amarra o this a classe que estou
        //sempre que adicionar um metodo novo que precisa acessar o estado ou mesmo chamar método dentro da classe
        //precisa ir no construtor e fazer o bind para evitar que o this seja null ou qualquer outro valor no fluxo da aplicação

        this.handleClear = this.handleClear.bind(this)
        this.handleSearch = this.handleSearch.bind(this)
        this.handleMarkAsDone = this.handleMarkAsDone.bind(this)
        this.handleMarkAsPending = this.handleMarkAsPending.bind(this)
        this.handleRemove = this.handleRemove.bind(this)
        this.refresh()
    }

    //Esse metodo vai pegar lista atualizada
    refresh(description = ''){
        const search = description ? `&description__regex=/${description}/` : ''
        axios.get(`${URL}?sort=-createdAt${search}`)
            .then(resp => this.setState({...this.state, description, list: resp.data} /*pega resultado altera estado*/))
    }

    //esse evento vai alterar o estado atual na parte referente a descrição
    handleChange(e){
        //esse é um ambiente controlado, quem manda aqui dentro não é mais a dom e sim o estado do componente
        this.setState({...this.state, description: e.target.value}) //operador sprad //ver mais sobre o setState
    }

    //essa função manipula o evento de adição de uma nova tarefa
    handleAdd(){
        //vai ser um ambiente controlado e adicionado um onChange para ser notificado das mudanças e manipular objeto 
        const description = this.state.description
        axios.post(URL, {description})
            .then(resp => this.refresh())
    }

    handleRemove(todo){
        axios.delete(`${URL}/${todo._id}`)
            .then(resp => this.refresh(this.state.description))
    }

    handleMarkAsDone(todo){
        axios.put(`${URL}/${todo._id}`, {...todo, done: true})
            .then(resp => this.refresh(this.state.description))
    }

    handleMarkAsPending(todo){
        axios.put(`${URL}/${todo._id}`, {...todo, done: false})
            .then(resp => this.refresh(this.state.description))
    }

    handleSearch(){
        this.refresh(this.state.description)
    }

    handleClear(){
        this.refresh()
    }

    render(){
        return (
            <div>
                <PageHeader name='Tarefas' small='Cadastro'></PageHeader>
                <TodoForm 
                    description={this.state.description} //sempre que o estado atualiza renderiza o formulario para refletir valor novo
                    handleChange={this.handleChange}
                    handleAdd={this.handleAdd}
                    handleSearch={this.handleSearch}
                    handleClear={this.handleClear}/>

                <TodoList 
                    list={this.state.list} 
                    handleMarkAsDone={this.handleMarkAsDone}
                    handleMarkAsPending={this.handleMarkAsPending}
                    handleRemove={this.handleRemove}/>
            </div>
        )
    }
}