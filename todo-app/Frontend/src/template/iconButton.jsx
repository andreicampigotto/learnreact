import React from 'react'

export default props => {
    // esse botão tem uma renderização condicional ele não vai retornar valor se tiver escondido
    if(props.hide){
        return null
    }
    else{
        return(
            // aqui contaceta parte de classe css com a propriedade do componente
            <button className={'btn btn-'+ props.style} 
                //oncClick também vem através das propriedades
                onClick={props.onClick}>

                <i className={'fa fa-'+ props.icon}></i>
            </button>
        )
    }
}