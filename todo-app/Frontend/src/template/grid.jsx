import React, {Component} from 'react'

export default class Grid extends Component{
    toCssClasses(numbers){

        //Aqui passa uma lista de numeros e converte para o padrão bootstrap
        //essesplit separa os numeros para considerar os 4 primeiros elementos do array
        //começa da tela menor para a maior
        const cols = numbers ? numbers.split(' ') : []
        let classes = ''

        if(cols[0]) classes += `col-xs-${cols[0]}` 
        if(cols[1]) classes += `col-sm-${cols[1]}`
        if(cols[2]) classes += `col-md-${cols[2]}`
        if(cols[3]) classes += `col-lg-${cols[3]}`

        return classes
    }

    //metodo para renderizar
    render(){
        //aqui espera receber as colunas se não for setado altomaticamente passa 12
        const gridClasses = this.toCssClasses(this.props.cols || 12)
        return(
            <div className={gridClasses}>
                {this.props.children}
            </div>
        )
    }
}